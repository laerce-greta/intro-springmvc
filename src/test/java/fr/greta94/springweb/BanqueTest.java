package fr.greta94.springweb;

import fr.greta94.springweb.domain.Banque;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by hippias on 20/01/2017.
 */
public class BanqueTest {
    @Test
    public void nomDeLaBanqueDoitBienEtrePrisEnCompte() {
        String nomPrevu = "Banque del Mundo";

        Banque banque = new Banque();
        banque.setNom(nomPrevu);

        assertThat(banque.getNom()).isEqualTo(nomPrevu);
    }

    //TODO: testez ici les méthodes de l'objet Banque
}
