package fr.greta94.springweb;

import fr.greta94.springweb.domain.Banque;
import fr.greta94.springweb.domain.Client;
import fr.greta94.springweb.domain.Compte;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by hippias on 20/01/2017.
 */
@Configuration
public class ApplicationConfiguration {

    @Bean
    public Banque banque() {
        Banque banqueAInjecter = new Banque();

        Client client1 = new Client();
        client1.setNumero("12345");
        client1.setNom("Dow");
        client1.setPrenom("Jones");
        client1.setSensible(false);

        Client client2 = new Client();
        client2.setNumero("67890");
        client2.setNom("Dow");
        client2.setPrenom("Jane");
        client2.setSensible(true);

        Compte compte1 = new Compte();
        compte1.setNumero("1234");
        compte1.setMontant(-10_000);

        Compte compte2 = new Compte();
        compte2.setNumero("atchoum");
        compte2.setMontant(123);

        Compte compte3 = new Compte();
        compte3.setNumero("hfeay feahfea");
        compte3.setMontant(43232323);


        banqueAInjecter.creerNouveauClient(client1);
        banqueAInjecter.creerNouveauClient(client2);

        banqueAInjecter.associerCompteAClient(client1, compte1);
        banqueAInjecter.associerCompteAClient(client2, compte2);
        banqueAInjecter.associerCompteAClient(client2, compte3);

        return banqueAInjecter;
    }

}
