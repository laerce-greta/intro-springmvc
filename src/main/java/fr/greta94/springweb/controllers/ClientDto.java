package fr.greta94.springweb.controllers;

import fr.greta94.springweb.domain.Client;

/**
 * Created by hippias on 23/01/2017.
 */
public class ClientDto {
    private String numero;
    private String nom;
    private String prenom;
    private boolean decouvert;

    public ClientDto(Client client, boolean decouvert) {
        this.numero = client.getNumero();
        this.nom = client.getNom();
        this.prenom = client.getPrenom();
        this.decouvert = decouvert;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public boolean isDecouvert() {
        return decouvert;
    }

    public void setDecouvert(boolean decouvert) {
        this.decouvert = decouvert;
    }

    @Override
    public String toString() {
        return "ClientDto{" +
                "numero='" + numero + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", decouvert=" + decouvert +
                '}';
    }
}
