package fr.greta94.springweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by hippias on 19/01/2017.
 */
//On indique à Spring que cette classe est un contrôleur
@Controller
public class BonjourController {

    @RequestMapping("/simpleBonjour")
    @ResponseBody
    public String simpleBonjour() {
        return "Hello Greta!";
    }

    //sur l'adresse /bonjour => voilà ce que l'on veut que l'application fasse
    @RequestMapping("/boujourDuTemplate")
    public String bonjour(@RequestParam(value="nom", required=false, defaultValue="Monde") String nom, //ici on récupère la valeur de l'attribut nom
                           Model model) { //injecté par spring cette objet permet de passer des infor mations au template
        model.addAttribute("nomTemplate", nom.toUpperCase()); //variable disponible dans le template
        // nom du template dans le répertoire templates
        return "bonjour";
    }

    @RequestMapping("/bonjourListe")
    public String bonjourListe(Model model) {
        List<String> liste = Collections.emptyList();
                //Arrays.asList("Buna", "Bonjour", "Hello", "Guten Tag");
        model.addAttribute("listeBonjour", liste);
        model.addAttribute("superVariable", "WOUHOU");

        return "bonjourListe";
    }

    @RequestMapping("/bonjourHash")
    public String bonjourHash(Model model) {
        Map<String, String> dicoBonjour = new HashMap<>();
        dicoBonjour.put("Bonjour", "français");
        dicoBonjour.put("Hello", "anglais");
        dicoBonjour.put("Buna", "roumain");
        dicoBonjour.put("Guten Tag", "allemand");

        model.addAttribute("hashBonjour", dicoBonjour.entrySet()); //nécessaire pour Mustache

        return "bonjourHash";
    }

    @RequestMapping("/bonjourBool")
    public String bonjourBool (@RequestParam(value="afficher") boolean afficher,
                                    Model model) {
        model.addAttribute("afficherBool", afficher);

        return "bonjourBool";
    }

    @RequestMapping("/bonjourDuController")
    @ResponseBody
    public String bonjourController(@RequestParam(value="nom", required=false, defaultValue="Monde") String nom, //ici on récupère la valeur de l'attribut nom
                                    @RequestParam(value="nombre", required=false, defaultValue="1") int nombre) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < nombre; i++) {
            buffer.append("Bonjour ");
            buffer.append(nom);
            buffer.append("\n");
        }

        return buffer.toString();
    }

}
