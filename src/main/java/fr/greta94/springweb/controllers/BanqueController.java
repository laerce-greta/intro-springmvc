package fr.greta94.springweb.controllers;

import fr.greta94.springweb.domain.Banque;
import fr.greta94.springweb.domain.Client;
import fr.greta94.springweb.domain.Compte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by hippias on 20/01/2017.
 */
@Controller
@RequestMapping("/banque")
public class BanqueController {

    @Autowired
    private Banque banque;

    @RequestMapping("") // "/banque"
    public String accueil(Model model) {
        model.addAttribute("banque", banque);

        return "banque/accueil";
    }

    @RequestMapping("clients") // "/banque/clients"
    public String listeClients(Model model) {
        List<ClientDto> clientsAffiches =
                banque.getListeClients().stream()
                    .map(c -> {
                        boolean decouvert =  banque.getListeComptesFromClient(c)
                                                .stream()
                                                .mapToDouble(Compte::getMontant).sum() < 0;
                        return  new ClientDto(c, decouvert);
                    }).collect(Collectors.toList());

        model.addAttribute("listeClients", clientsAffiches);

        return "banque/clients";
    }

    @RequestMapping("client/{numeroClient}/comptes") // "/banque/clients/comptes"
    public String listeCompteClient(@PathVariable(value = "numeroClient") String numeroClient, //utiliser PathVariable
                                    Model model) {

        Client client = banque.getClientFromNumero(numeroClient);
        List<Compte> comptesClient = banque.getListeComptesFromClient(client);
        double soldeClient = comptesClient.stream().mapToDouble(Compte::getMontant).sum();


        model.addAttribute("client", client);
        model.addAttribute("solde", soldeClient);
        model.addAttribute("comptes", comptesClient);

        return "banque/comptesClient";
    }

    @RequestMapping("comptes") // "/banque/comptes"
    public String listeComptes(Model model) {
        model.addAttribute("comptes", banque.getListeComptes());

        return "banque/comptes";
    }



    @RequestMapping("clientsJson")
    public @ResponseBody List<ClientDto> listeClientsJson() {
        return banque.getListeClients().stream()
                        .map(c -> {
                            boolean decouvert =  banque.getListeComptesFromClient(c)
                                    .stream()
                                    .mapToDouble(Compte::getMontant).sum() < 0;
                            return  new ClientDto(c, decouvert);
                        }).collect(Collectors.toList());
    }

    @RequestMapping("nom")
    @ResponseBody
    public String nomDeLaBanque() {
        return banque.getNom();
    }






    //gérer affichage compte sur page client en dynamique avec jQuery

    //permettre création de compte et réalisation de mouvement
}
