package fr.greta94.springweb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by hippias on 23/01/2017.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClientNotFoundException extends RuntimeException {
    public ClientNotFoundException(String numeroClient) {
        super(String.join(" ", "Le numéro client demandé:", numeroClient, "n'a pas été trouvé"));
    }
}
