package fr.greta94.springweb.domain;

/**
 * Created by hippias on 20/01/2017.
 */
// Mettre en place les annotations de validation
public class Compte {
    private String numero;
    private double montant;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Compte compte = (Compte) o;

        return numero != null ? numero.equals(compte.numero) : compte.numero == null;

    }

    @Override
    public int hashCode() {
        return numero != null ? numero.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "numero='" + numero + '\'' +
                ", montant=" + montant +
                '}';
    }
}
