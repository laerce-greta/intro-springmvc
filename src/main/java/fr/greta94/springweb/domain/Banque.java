package fr.greta94.springweb.domain;

import fr.greta94.springweb.controllers.ClientNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by hippias on 20/01/2017.
 */
@Component
@Scope("singleton")
public class Banque {

    @Value("${mabanque.nom}")
    private String nom;

    private Map<Client, List<Compte>> carnetDeComptes;

    public Banque() {
        this.carnetDeComptes = new HashMap<>();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void creerNouveauClient(Client client) {
        if (!carnetDeComptes.containsKey(client))
            carnetDeComptes.put(client, new ArrayList<>());
    }

    public void associerCompteAClient(Client client, Compte compte) {
        if (!carnetDeComptes.containsKey(client))
            creerNouveauClient(client);

        carnetDeComptes.get(client).add(compte);
    }

    public Set<Client> getListeClients() {
        return carnetDeComptes.keySet();
    }

    public List<Compte> getListeComptesBasique() {
        Collection<List<Compte>> listeListeCompte = carnetDeComptes.values();

        List<Compte> compteAgreges = new ArrayList<>();
        for (List<Compte> listeCompte: listeListeCompte) {
            compteAgreges.addAll(listeCompte);
        }

        return compteAgreges;
    }

    public List<Compte> getListeComptes() {
        return carnetDeComptes.values().stream()
                .flatMap(comptes -> comptes.stream())
                .collect(Collectors.toList());
    }


    public Client getClientFromNumero(String numeroClient) {
        Optional<Client> optionalClient = carnetDeComptes.keySet().stream()
                .filter(c -> c.getNumero().equals(numeroClient))
                .findFirst();

        if (!optionalClient.isPresent())
            throw new ClientNotFoundException(numeroClient);


        return optionalClient.get();
    }

    public List<Compte> getListeComptesFromClient(Client client) {
        return carnetDeComptes.get(client);
    }
}
