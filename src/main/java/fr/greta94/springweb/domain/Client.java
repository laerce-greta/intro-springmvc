package fr.greta94.springweb.domain;

/**
 * Created by hippias on 20/01/2017.
 */
public class Client {
    private String numero;
    private String nom;
    private String prenom;
    private boolean sensible;

    public Client() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public boolean isSensible() {
        return sensible;
    }

    public void setSensible(boolean sensible) {
        this.sensible = sensible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        return numero != null ? numero.equals(client.numero) : client.numero == null;

    }

    @Override
    public int hashCode() {
        return numero != null ? numero.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Client{" +
                "numero='" + numero + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", sensible=" + sensible +
                '}';
    }
}
